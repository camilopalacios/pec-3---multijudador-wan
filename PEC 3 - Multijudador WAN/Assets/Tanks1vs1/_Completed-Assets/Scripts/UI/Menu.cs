﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public void TwoPlayers()
    {
        // Guardamos el número de jugadores
        PlayerPrefs.SetInt("Players", 2);

        // Se nos redirigirá a la siguiente escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ThreePlayers()
    {
        // Guardamos el número de jugadores
        PlayerPrefs.SetInt("Players", 3);

        // Se nos redirigirá a la siguiente escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void FourPlayers()
    {
        // Guardamos el número de jugadores
        PlayerPrefs.SetInt("Players", 4);

        // Se nos redirigirá a la siguiente escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
