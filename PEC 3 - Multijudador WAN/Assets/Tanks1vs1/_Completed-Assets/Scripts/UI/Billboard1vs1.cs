using UnityEngine;
using System.Collections;

public class Billboard1vs1 : MonoBehaviour
{

  // Update is called once per frame
  void Update()
  {
    if (Camera.main != null)
    {
      transform.LookAt(Camera.main.transform);
    }
    else
    {
      Camera cam = transform.parent.GetComponentInChildren<Camera>();
      if (cam != null) transform.LookAt(cam.transform);
    }
  }
}
