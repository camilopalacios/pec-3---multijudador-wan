using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {
        public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases.
        public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control.
        public TankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks.

        
        private int m_RoundNumber;                  // Which round the game is currently on.
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won.
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won.
        private Camera mainCam;                     // Main camera
        private GameObject playersCanvas;           // El canvas que contiene los botones de inicio de los jugadores
        private bool[] playersActive;               // Aparecen los jugadores que est�n o no activos
        private int players;                        // N�mero actual de jugadores


        private void Start()
        {
            // Consultamos el n�mero actual de jugadores
            players = PlayerPrefs.GetInt("Players");

            playersActive = new bool[4] { false, false, false, false };
            playersCanvas = GameObject.Find("PlayersCanvas");

            Debug.Log("Players: " + players.ToString());

            // Create the delays so they only have to be made once.
            m_StartWait = new WaitForSeconds (m_StartDelay);
            m_EndWait = new WaitForSeconds (m_EndDelay);

            SpawnAllTanks();
            SetCameraTargets();

            for (int i = 0; i < players; ++i)
            {
                // Ocultamos los botones de inicio de los jugadores que est�n activos
                playersCanvas.transform.GetChild(i).gameObject.SetActive(false);

                // Actualizamos el vector de jugadores activos, con 'true' para los activos y 'false' para los no activos
                playersActive[i] = true;
            }

            // Once the tanks have been created and the camera is using them as targets, start the game.
            StartCoroutine (GameLoop ());
        }

        private void SpawnAllTanks()
        {
            mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();

            // For all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... create them, set their player number and references needed for control.
                EnableTank(i, mainCam);
            }

            // Si el n�mero actual de jugadores activos es 3
            if (PlayerPrefs.GetInt("Players") == 3)
            {
                AddMiniMap(mainCam);
            }
            else
            {
                DisableMiniMap();
            }

            mainCam.gameObject.SetActive(false);
        }

        private void EnableTank(int i, Camera mainCam)
        {
            m_Tanks[i].m_Instance =
                    Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
            m_Tanks[i].m_PlayerNumber = i + 1;
            m_Tanks[i].Setup();
            AddCamera(i, mainCam);            
        }

        private void AddCamera(int i, Camera mainCam) {


			GameObject childCam = new GameObject( "Camera"+(i+1) );
			Camera newCam = childCam.AddComponent<Camera>();		
			newCam.CopyFrom(mainCam);
            
			childCam.transform.parent = m_Tanks[i].m_Instance.transform;

            // Redimensionamos la SplitScreen
            Resize(i, newCam);
        }

        private void AddMiniMap(Camera mainCam)
        {
            Camera minimapCamera = GameObject.Find("MiniMapPoint").transform.GetChild(0).GetComponent<Camera>();
            
            minimapCamera.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
            minimapCamera.gameObject.SetActive(true);
        }

        private void DisableMiniMap()
        {
            Camera minimapCamera = GameObject.Find("MiniMapPoint").transform.GetChild(0).GetComponent<Camera>();
            minimapCamera.gameObject.SetActive(false);
        }

        private void SetCameraTargets()
        {
            // Create a collection of transforms the same size as the number of tanks.
            Transform[] targets = new Transform[m_Tanks.Length];

            // For each of these transforms...
            for (int i = 0; i < targets.Length; i++)
            {
                // ... set it to the appropriate tank transform.
                targets[i] = m_Tanks[i].m_Instance.transform;
            }

            // These are the targets the camera should follow.
            m_CameraControl.m_Targets = targets;
        }


        // This is called from start and will run each phase of the game one after another.
        private IEnumerator GameLoop ()
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
            yield return StartCoroutine (RoundStarting ());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
            yield return StartCoroutine (RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
            yield return StartCoroutine (RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level.
                SceneManager.LoadScene (0);
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues.
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
                StartCoroutine (GameLoop ());
            }
        }


        private IEnumerator RoundStarting ()
        {

            // As soon as the round starts reset the tanks and make sure they can't move.
            ResetAllTanks ();
            DisableTankControl ();

            //Actualizamos el n�mero de jugadores activos

            PlayerPrefs.SetInt("Players", players);

            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // Si el jugador no est� activo
                if (!playersActive[i])
                {
                    // Desactivamos el tanque correspondiente al jugador
                    m_Tanks[i].m_Instance.SetActive(false);

                    // Desactivamos los controles correspondientes al jugador
                    m_Tanks[i].DisableControl();

                    // Mostramos el bot�n de inicio para el jugador correspondiente
                    playersCanvas.transform.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    // Ocultamos el bot�n de inicio para el jugador correspondiente
                    playersCanvas.transform.GetChild(i).gameObject.SetActive(false);
                }
            }

            // Redimensionamos la SplitScreen
            ResizeSplitScreen(true);

            // Snap the camera's zoom and position to something appropriate for the reset tanks.
            m_CameraControl.SetStartPositionAndSize ();

            // Increment the round number and display text showing the players what round it is.
            m_RoundNumber++;
            m_MessageText.text = "ROUND " + m_RoundNumber;

            // Wait for the specified length of time until yielding control back to the game loop.
            yield return m_StartWait;
        }


        private IEnumerator RoundPlaying ()
        {
            // As soon as the round begins playing let the players control the tanks.
            EnableTankControl ();

            //ResizeSplitScreen();

            // Clear the text from the screen.
            m_MessageText.text = string.Empty;

            // While there is not one tank left...
            while (!OneTankLeft())
            {
                // ... return on the next frame.
                yield return null;
            }
        }


        private IEnumerator RoundEnding ()
        {
            // Stop tanks from moving.
            DisableTankControl ();

            // Clear the winner from the previous round.
            m_RoundWinner = null;

            // See if there is a winner now the round is over.
            m_RoundWinner = GetRoundWinner ();

            // If there is a winner, increment their score.
            if (m_RoundWinner != null)
                m_RoundWinner.m_Wins++;

            // Now the winner's score has been incremented, see if someone has one the game.
            m_GameWinner = GetGameWinner ();

            // Get a message based on the scores and whether or not there is a game winner and display it.
            string message = EndMessage ();
            m_MessageText.text = message;

            // Wait for the specified length of time until yielding control back to the game loop.
            yield return m_EndWait;
        }


        // This is used to check if there is one or fewer tanks remaining and thus the round should end.
        private bool OneTankLeft()
        {
            // Start the count of tanks left at zero.
            int numTanksLeft = 0;

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if they are active, increment the counter.
                if (m_Tanks[i].m_Instance.activeSelf)
                    numTanksLeft++;
                else {
                    // Si el jugador es un jugador activo
                    if (playersActive[i])
                    {
                        // Desactivamos el jugador
                        DisablePlayer(i);
                    }
                }
            }

            // If there are one or fewer tanks remaining return true, otherwise return false.
            return numTanksLeft <= 1;
        }
        
        
        // This function is to find out if there is a winner of the round.
        // This function is called with the assumption that 1 or fewer tanks are currently active.
        private TankManager GetRoundWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them is active, it is the winner so return it.
                if (m_Tanks[i].m_Instance.activeSelf)
                    return m_Tanks[i];
            }

            // If none of the tanks are active it is a draw so return null.
            return null;
        }


        // This function is to find out if there is a winner of the game.
        private TankManager GetGameWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it.
                if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                    return m_Tanks[i];
            }

            // If no tanks have enough rounds to win, return null.
            return null;
        }


        // Returns a string message to display at the end of each round.
        private string EndMessage()
        {
            // By default when a round ends there are no winners so the default end message is a draw.
            string message = "DRAW!";

            // If there is a winner then change the message to reflect that.
            if (m_RoundWinner != null)
                message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

            // Add some line breaks after the initial message.
            message += "\n\n\n\n";

            // Go through all the tanks and add each of their scores to the message.
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if (playersActive[i])
                {
                    message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
                }
            }

            // If there is a game winner, change the entire message to reflect that.
            if (m_GameWinner != null)
                message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

            return message;
        }

        // This function is used to turn all the tanks back on and reset their positions and properties.
        private void ResetAllTanks()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                m_Tanks[i].Reset();
            }
        }

        private void EnableTankControl()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                m_Tanks[i].EnableControl();
            }
        }

        private void DisableTankControl()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                m_Tanks[i].DisableControl();
            }
        }

        private void EnablePlayer(int i)
        {
            // Reseteamos el tanque correspondiente
            m_Tanks[i].Reset();

            // Si la SplitScreen no est� activada
            if (mainCam.gameObject.activeSelf)
            {
                // Desactivamos la c�mara principal
                GameObject.Find("CameraRig").transform.GetChild(0).gameObject.SetActive(false);

                // Redimensionamos la SplitScreen
                ResizeSplitScreen(false);

                // Activamos la c�mara principal
                GameObject.Find("CameraRig").transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                // Redimensionamos la SplitScreen
                ResizeSplitScreen(true);
            }
        }

        private void DisablePlayer(int i)
        {
            // Si el jugador acaba de morir
            if (!playersCanvas.transform.GetChild(i).gameObject.activeSelf)
            {
                // Mostramos el bot�n de inicio del jugador correspondiente
                playersCanvas.transform.GetChild(i).gameObject.SetActive(true);

                // Si la SplitScreen no est� activada
                if (mainCam.gameObject.activeSelf)
                {
                    // Desactivamos la c�mara principal
                    GameObject.Find("CameraRig").transform.GetChild(0).gameObject.SetActive(false);

                    // Redimensionamos la SplitScreen
                    ResizeSplitScreen(false);

                    // Activamos la c�mara principal
                    GameObject.Find("CameraRig").transform.GetChild(0).gameObject.SetActive(true);
                }
                else
                {
                    // Redimensionamos la SplitScreen
                    ResizeSplitScreen(true);
                }
            }
        }

        private void ResizeSplitScreen(bool isSplit)
        {
            int i = 0;
            for (int j = 0; j < m_Tanks.Length; j++)
            {
                if (m_Tanks[j].m_Instance.activeSelf)
                {
                    Camera newCam = m_Tanks[j].m_Instance.GetComponentInChildren<Camera>();
                    Resize(i, newCam);
                    i++;
                }
            }
            // Si est� activa la SplitScreen
            if (isSplit)
            {
                if (PlayerPrefs.GetInt("Players") == 3)
                {
                    AddMiniMap(mainCam);
                }
                else
                {
                    DisableMiniMap();
                }
            }
        }

        public void Resize(int i, Camera newCam)
        {
            if (PlayerPrefs.GetInt("Players") > 2)
            {
                switch (i)
                {
                    case 0:
                        newCam.rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                        break;
                    case 1:
                        newCam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                        break;
                    case 2:
                        newCam.rect = new Rect(0f, 0f, 0.5f, 0.5f);
                        break;
                    case 3:
                        newCam.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                        break;
                }
            }
            else
            {
                if (i == 0) newCam.rect = new Rect(0.0f, 0.5f, 1.0f, 0.5f);
                else newCam.rect = new Rect(0f, 0.0f, 1.0f, 0.5f);
            }
        }

        public void AddP1Tank()
        {
            // Actualizamos el n�mero actual de jugadores y lo guardamos
            PlayerPrefs.SetInt("Players", PlayerPrefs.GetInt("Players") + 1);
            EnablePlayer(0);

            // Ocultamos el bot�n de inicio del jugador 1
            playersCanvas.transform.GetChild(0).gameObject.SetActive(false);
        }

        public void AddP2Tank()
        {
            // Actualizamos el n�mero actual de jugadores y lo guardamos
            PlayerPrefs.SetInt("Players", PlayerPrefs.GetInt("Players") + 1);
            EnablePlayer(1);
            // Ocultamos el bot�n de inicio del jugador 2
            playersCanvas.transform.GetChild(1).gameObject.SetActive(false);
        }

        public void AddP3Tank()
        {
            // Actualizamos el n�mero actual de jugadores y lo guardamos
            PlayerPrefs.SetInt("Players", PlayerPrefs.GetInt("Players") + 1);

            // Si el jugador 3 no hab�a iniciado antes partida
            if (!playersActive[2])
            {
                playersActive[2] = true;
                players++;
            }

            EnablePlayer(2);

            // Ocultamos el bot�n de inicio del jugador 3
            playersCanvas.transform.GetChild(2).gameObject.SetActive(false);
        }

        public void AddP4Tank()
        {
            // Actualizamos el n�mero actual de jugadores y lo guardamos
            PlayerPrefs.SetInt("Players", PlayerPrefs.GetInt("Players") + 1);

            // Si el jugador 4 no hab�a iniciado antes partida
            if (!playersActive[3])
            {
                playersActive[3] = true;
                players++;
            }

            EnablePlayer(3);

            // Ocultamos el bot�n de inicio del jugador 4
            playersCanvas.transform.GetChild(3).gameObject.SetActive(false);
        }
    }
}