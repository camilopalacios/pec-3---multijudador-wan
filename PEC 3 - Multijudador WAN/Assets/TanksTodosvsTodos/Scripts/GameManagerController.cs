﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerController : MonoBehaviour
{
  public Canvas playerCanvas;
  public InputField nicknameField;

  public void SaveChanges()
  {
    string nickname = nicknameField.text;
    PlayerPrefs.SetString("Nickname", nickname);

    CUIColorPicker colorpicker = GameObject.Find("CUIColorPicker").GetComponent<CUIColorPicker>();
    Color userColor = colorpicker.Color;

    PlayerPrefs.SetFloat("UserColorR", userColor.r);
    PlayerPrefs.SetFloat("UserColorG", userColor.g);
    PlayerPrefs.SetFloat("UserColorB", userColor.b);

    TankController.isUpdated = false;
    // GameObject[] otherTanks = GameObject.FindGameObjectsWithTag("Player");
    // foreach (GameObject tank in otherTanks)
    // {
    //   if(tank.GetComponent<TankController>().isLocalPlayer) tank.GetComponent<TankController>().RefreshSettings();
    // }

    Return();
  }

  public void Return()
  {
    playerCanvas.gameObject.SetActive(false);
  }

  public void QuitGame()
  {
    Application.Quit();
  }

  public void Mute()
  {
    GameObject.Find("CameraRig").GetComponent<AudioSource>().mute = !GameObject.Find("CameraRig").GetComponent<AudioSource>().mute;
  }
}
