using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyMenu : MonoBehaviour {

    LobbyDiscovery discovery;

    private bool isJoin;
    private bool isInitialServer;
    public InputField nicknameField;
    public Canvas playerCanvas;
    public Canvas serverCanvas;
	private string IPServer = "";
	public InputField IPNumber1;
	public InputField IPNumber2;
	public InputField IPNumber3;
	public InputField IPNumber4;
	public InputField IPPort;

    void Awake() {

        discovery = FindObjectOfType<LobbyDiscovery>();
    }

    void Start() {

        StopDiscovery();
        isJoin = false;
        isInitialServer = false;
    }

    public void RunServer() {
        gameObject.SetActive(false);
        serverCanvas.gameObject.SetActive(true);
        serverCanvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);

        
        isJoin = false;
        isInitialServer = true;
    }

    public void CreateGame() {
        gameObject.SetActive(false);
        playerCanvas.gameObject.SetActive(true);
        isJoin = false;
        isInitialServer = false;
    }

    public void JoinGame() {

        gameObject.SetActive(false);
        playerCanvas.gameObject.SetActive(true);
        isJoin = true;
        isInitialServer = false;
    }

    private void StopDiscovery() {

        if (discovery.running) {
            discovery.StopBroadcast();
        }
    }

    private void AddressData() {
        // Obsolete in Unity 2018.3
        // Debug.Log("Network ip: " + Network.natFacilitatorIP);

        // Obsolete in Unity 2018.3
        // Debug.Log("Network port: " + Network.natFacilitatorPort);
        int portN = int.Parse(IPPort.text);
		NetworkManager.singleton.networkPort = portN;

        Debug.Log("Network ip: " + NetworkManager.singleton.networkAddress);
        Debug.Log("Network port: " + NetworkManager.singleton.networkPort);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer ip: " + Network.player.ipAddress);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer port: " + Network.player.port);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer external ip: " + Network.player.externalIP);

        // Obsolete in Unity 2018.3
        //Debug.Log("Networkplayer external port: " + Network.player.externalPort);
    }

    public void StartClient(){
        StopDiscovery();


		    string address = IPNumber1.text + "." + IPNumber2.text + "." + IPNumber3.text + "." + IPNumber4.text;
		    int portN = int.Parse(IPPort.text);

		    NetworkManager.singleton.networkAddress = address;

        // In order to allow LAN connectivity ** ONLY ** this line should be active
        // discovery.StartAsClient();

        AddressData();
        // In order to allow localhost multiplayer (multiple instances of the game running in just one computer, ** ONLY ** this line should be active
        NetworkManager.singleton.StartClient();

    }

    public void SaveProfile()
    {
        string nickname;

        if (nicknameField.text == ""){
            nickname = string.Format("Player{0}", this.GetInstanceID());
        }
        else {
            nickname = nicknameField.text;
        }

        CUIColorPicker colorpicker = GameObject.Find("CUIColorPicker").GetComponent<CUIColorPicker>();
        Color userColor = colorpicker.Color;

        PlayerPrefs.SetFloat("UserColorR", userColor.r);
        PlayerPrefs.SetFloat("UserColorG", userColor.g);
        PlayerPrefs.SetFloat("UserColorB", userColor.b);
        PlayerPrefs.SetString("Nickname", nickname);

        playerCanvas.gameObject.SetActive(false);
        serverCanvas.gameObject.SetActive(true);

        if (!isJoin) //Creamos partida deshabilitamos la IP
        {
			serverCanvas.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    public void SaveServerInfo()
    {
        if (isJoin)
        {
            StartClient();
        }
        else
        {
            if (isInitialServer)
            {
                AddressData();
                StopDiscovery();
                discovery.StartAsServer();
                NetworkManager.singleton.StartServer();
            }
            else
            {
                AddressData();
                StopDiscovery();
                discovery.StartAsServer();
                NetworkManager.singleton.StartHost();
            }

        }
    }

    public void Return()
    {
        gameObject.SetActive(true);
        playerCanvas.gameObject.SetActive(false);
    }

    public void ReturnProfile()
    {
        if (isJoin) //Unirse partida
        {
            serverCanvas.gameObject.transform.GetChild(1).gameObject.SetActive(true);
            playerCanvas.gameObject.SetActive(true);
        }
        else
        {
            if (isInitialServer) // Iniciar servidor
            {
                serverCanvas.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                gameObject.SetActive(true);
            } else
            {
                serverCanvas.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                playerCanvas.gameObject.SetActive(true);
            }
        }
        serverCanvas.gameObject.SetActive(false);
    }
}