using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class TankController : NetworkBehaviour
{
  public GameObject bulletPrefab;
  public GameObject fastBulletPrefab;
  public Transform bulletSpawn;

  private GameObject playerCanvas;

  [SerializeField]
  [SyncVar(hook = "OnChangeColorR")]
  public float colorR = 1.0f;

  [SerializeField]
  [SyncVar(hook = "OnChangeColorG")]
  public float colorG = 1.0f;

  [SerializeField]
  [SyncVar(hook = "OnChangeColorB")]
  public float colorB = 1.0f;

  [SerializeField]
  private Text nicknameText;

  [SerializeField]
  [SyncVar(hook = "OnChangeNickname")]
  public string nickname = "new player";
  public static bool isUpdated = true;

  void Update()
  {
    if (!isLocalPlayer) return;

    if (!isUpdated)
    {
      UpdateLocalValues();
      CmdUpdateTankPrefs(PlayerPrefs.GetString("Nickname"), PlayerPrefs.GetFloat("UserColorR"), PlayerPrefs.GetFloat("UserColorG"), PlayerPrefs.GetFloat("UserColorB"));
      isUpdated = true;
    }
    var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
    var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

    transform.Rotate(0, x, 0);
    transform.Translate(0, 0, z);

    if (Input.GetKeyDown(KeyCode.Space))
    {
      CmdFire();
    }

    if (Input.GetKeyDown(KeyCode.LeftShift))
    {
      CmdFastFire();
    }

    if (Input.GetKeyDown(KeyCode.Escape))
    {
      if (!playerCanvas.activeSelf)
      {
        playerCanvas.GetComponentInChildren<InputField>().text = nickname;
        CUIColorPicker colorpicker = playerCanvas.GetComponentInChildren<CUIColorPicker>();
        colorpicker.Color = new Color(colorR, colorG, colorB);
      }
      playerCanvas.SetActive(!playerCanvas.activeSelf);
    }
  }

  public override void OnStartLocalPlayer()
  {
    base.OnStartLocalPlayer();
    PreparePlayerCanvas();
    UpdateLocalValues();
    CmdUpdateTankPrefs(PlayerPrefs.GetString("Nickname"), PlayerPrefs.GetFloat("UserColorR"), PlayerPrefs.GetFloat("UserColorG"), PlayerPrefs.GetFloat("UserColorB"));
    StartCoroutine(RefreshAllTanksNickname());
  }

  // Used by the client to ask the server to update the local player's current settings (color and nickname)
  [Command]
  void CmdUpdateTankPrefs(string newNickname, float newColorR, float newColorG, float newColorB)
  {
    if (!isServer) return;
    ChangeNickname(newNickname);
    ChangeColorR(newColorR);
    ChangeColorG(newColorG);
    ChangeColorB(newColorB);
    RpcUpdateTankPrefs();
  }

  // Used by the server to tell all the players to update their color
  [ClientRpc]
  void RpcUpdateTankPrefs()
  {
    StartCoroutine(RefreshAllTanksColor());
  }

  // This methods are triggered when one component of the player's color changes its value
  public void OnChangeNickname(string newNickname)
  {
    RefreshNickname(newNickname);
  }
  // Only the server uses this methods to update the player's nickname value
  public void ChangeNickname(string value)
  {
    if (!isServer) return;
    nickname = value;
  }

  // This methods are triggered when one component of the player's color changes its value
  public void OnChangeColorR(float newColorR)
  {
    colorR = newColorR;
  }
  public void OnChangeColorG(float newColorG)
  {
    colorG = newColorG;
  }
  public void OnChangeColorB(float newColorB)
  {
    colorB = newColorB;
  }
  // Only the server uses this methods to update the color values
  public void ChangeColorR(float value)
  {
    if (!isServer) return;
    colorR = value;
  }
  public void ChangeColorG(float value)
  {
    if (!isServer) return;
    colorG = value;
  }
  public void ChangeColorB(float value)
  {
    if (!isServer) return;
    colorB = value;
  }

  IEnumerator RefreshAllTanksColor()
  {
    yield return new WaitForSeconds(1f);
    GameObject[] otherTanks = GameObject.FindGameObjectsWithTag("Player");
    foreach (GameObject tank in otherTanks)
    {
      tank.GetComponent<TankController>().RefreshColor();
    }
  }

  IEnumerator RefreshAllTanksNickname()
  {
    yield return new WaitForSeconds(1f);
    GameObject[] otherTanks = GameObject.FindGameObjectsWithTag("Player");
    foreach (GameObject tank in otherTanks)
    {
      string tanksNickname = tank.GetComponent<TankController>().GetNickname();
      tank.GetComponent<TankController>().RefreshNickname(tanksNickname);
    }
  }
  
  public void RefreshColor()
  {
    foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
    {
      child.material.color = new Color(colorR, colorG, colorB);
    }
  }

  private void RefreshNickname(string value)
  {
    nicknameText.text = value;
  }

  [Command]
  void CmdFire()
  {
    // Create the Bullet from the Bullet Prefab
    var bullet = (GameObject)Instantiate(bulletPrefab,bulletSpawn.position,bulletSpawn.rotation);
    // Add velocity to the bullet
    bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
    // Spawn the bullet on the Clients
    NetworkServer.Spawn(bullet);
    // Destroy the bullet after 2 seconds
    Destroy(bullet, 2.0f);
  }

  [Command]
  void CmdFastFire()
  {
    // Create the Bullet from the Fast Bullet Prefab
    var bullet = (GameObject)Instantiate(fastBulletPrefab,bulletSpawn.position,bulletSpawn.rotation);
    // Add velocity to the bullet
    bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 15;
    // Spawn the bullet on the Clients
    NetworkServer.Spawn(bullet);
    // Destroy the bullet after 2 seconds
    Destroy(bullet, 2.0f);
  }

  void PreparePlayerCanvas(){
    playerCanvas = GameObject.FindWithTag("PlayerCanvas");
    if (playerCanvas != null)
    {
      playerCanvas.SetActive(false);
    }
  }

  void UpdateLocalValues(){
    nickname = PlayerPrefs.GetString("Nickname");
    colorR = PlayerPrefs.GetFloat("UserColorR");
    colorG = PlayerPrefs.GetFloat("UserColorG");
    colorB = PlayerPrefs.GetFloat("UserColorB");
  }

  public string GetNickname(){
    return nickname;
  }
}
