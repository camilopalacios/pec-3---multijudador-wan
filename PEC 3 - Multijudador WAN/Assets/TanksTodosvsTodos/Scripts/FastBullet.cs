using UnityEngine;

public class FastBullet : MonoBehaviour {

    void OnCollisionEnter(Collision collision) {
		
        GameObject hit = collision.gameObject;
		Health health = hit.GetComponent<Health>();
        if (health != null) {
            health.TakeDamage(5);
        }

        Destroy(gameObject);
    }
}
