﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ChangeColor2vs2 : NetworkBehaviour {
	public Texture botonVerde;
	public Texture botonAzul;

	[SyncVar(hook = "OnChangeColor")]
	public string m_PlayerColor = "Rojo";

	void Start() {
		OnChangeColor (m_PlayerColor);
	}

	void OnGUI() {
		if (isLocalPlayer) {
			// LOS BOTONES SOLO SERAN UTILES CUANDO EL PLAYER NO HAYA ELEGIDO COLOR
			// CREAMOS LA BOTONERA Y EL CONTADOR DE TANQUES VERDES
			if (GUI.Button (new Rect (200, 15, 25, 25), botonVerde, GUIStyle.none) & m_PlayerColor == "Rojo")
				CmdChangeColor("Verde");

			// CREAMOS LA BOTONERA Y EL CONTADOR DE TANQUES AZULES
			if (GUI.Button (new Rect (240, 15, 25, 25), botonAzul, GUIStyle.none) & m_PlayerColor == "Rojo")
				CmdChangeColor("Azul");
		}
	}

	//punto 1 : actualizar servidor
	[Command]
	void CmdChangeColor(string Nombre) {
		// CONTROLAMOS QUE NO HAYAN MAS DE 2 JUGADORES POR EQUIPO
		if ((Nombre == "Verde" && CounterManager2vs2.singleton.m_Verdes == "2") ||
			(Nombre == "Azul" && CounterManager2vs2.singleton.m_Azules == "2")) {
			Debug.Log("No se puede añadir otro jugador con a ese equipo");
		} else {
			Color ColorUsado = Color.blue;
			switch (Nombre) {
			case "Azul":
				ColorUsado = Color.blue;
				break;
			case "Verde":
				ColorUsado = Color.green;
				break;
			}

			m_PlayerColor = Nombre;

			CounterManager2vs2.singleton.ActualizarContadores ();

			// COLOREAMOS EL TANQUE DEL COLOR ELEGIDO
			foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>()) {
				child.material.color = ColorUsado;
			}
		}
	}



	//punto 2 : actualizar clientes
	public void OnChangeColor(string Nombre) {
		if (Nombre != "Rojo") {
			Color ColorUsado = Color.blue;
			switch (Nombre) {
			case "Azul":
				ColorUsado = Color.blue;
				break;
			case "Verde":
				ColorUsado = Color.green;
				break;
			}

			m_PlayerColor = Nombre;

			// COLOREAMOS EL TANQUE DEL COLOR ELEGIDO
			foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>()) {
				child.material.color = ColorUsado;
			}
		}
	}
}
