using UnityEngine;
using UnityEngine.Networking;

public class TankController2vs2 : NetworkBehaviour
{
    public GameObject bulletPrefab;
	public GameObject bulletVerdePrefab;
	public GameObject bulletAzulPrefab;
    public GameObject fastBullet;
    public Transform bulletSpawn;
	private Camera mainCam;

    void Update() {
        if (!isLocalPlayer) {

            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown(KeyCode.Space))
        {
			if (this.gameObject.GetComponent<ChangeColor2vs2> ().m_PlayerColor != "Rojo") {
				CmdFire ();
			}
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (this.gameObject.GetComponent<ChangeColor2vs2>().m_PlayerColor != "Rojo")
            {
                CmdFastFire();
            }
        }
    }

    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdFire() {
		GameObject bullet;

		if (this.gameObject.GetComponent<ChangeColor2vs2> ().m_PlayerColor == "Verde") {
				// Create the Bullet from the Bullet Prefab
				bullet = (GameObject)Instantiate (
					bulletVerdePrefab,
					bulletSpawn.position,
					bulletSpawn.rotation);
		} else {
				// Create the Bullet from the Bullet Prefab
				bullet = (GameObject)Instantiate (
					bulletAzulPrefab,
					bulletSpawn.position,
					bulletSpawn.rotation);
		}

		// Add velocity to the bullet
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * 6;

		// Spawn the bullet on the Clients
		NetworkServer.Spawn (bullet);

		// Destroy the bullet after 2 seconds
		Destroy (bullet, 2.0f);

    }

    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdFastFire()
    {
        GameObject bullet;

        bullet = (GameObject)Instantiate(
              fastBullet,
              bulletSpawn.position,
              bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 15;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);

    }


    public override void OnStartLocalPlayer() {
		mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();

	}




}
