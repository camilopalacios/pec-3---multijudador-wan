using UnityEngine;
using UnityEngine.Networking;

public class LobbyMenu2vs2 : MonoBehaviour {

	LobbyDiscovery2vs2 discovery;

    void Awake() {
		
		discovery = FindObjectOfType<LobbyDiscovery2vs2>();
    }

    void Start() {
		
        StopDiscovery();
    }

	public void RunServer() {
		StopDiscovery();
		discovery.StartAsServer();
		NetworkManager.singleton.StartServer();
        AddressData();
    }

    public void CreateGame() {
        StopDiscovery();
        discovery.StartAsServer();
        NetworkManager.singleton.StartHost();
        AddressData();
    }

    public void JoinGame() {
        StopDiscovery();

        // In order to allow LAN connectivity ** ONLY ** this line should be active
        // discovery.StartAsClient();

        // In order to allow localhost multiplayer (multiple instances of the game running in just one computer, ** ONLY ** this line should be active
		NetworkManager.singleton.StartClient();

        AddressData();
    }

    private void StopDiscovery() {
		
        if (discovery.running) {
            discovery.StopBroadcast();
        }
    }

	private void AddressData() 	{
        // Obsolete in Unity 2018.3
        // Debug.Log("Network ip: " + Network.natFacilitatorIP);

        // Obsolete in Unity 2018.3
        // Debug.Log("Network port: " + Network.natFacilitatorPort);

        Debug.Log ("Network ip: " + NetworkManager.singleton.networkAddress);
        Debug.Log ("Network port: " + NetworkManager.singleton.networkPort);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer ip: " + Network.player.ipAddress);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer port: " + Network.player.port);

        // Obsolete in Unity 2018.3
        // Debug.Log("Networkplayer external ip: " + Network.player.externalIP);

        // Obsolete in Unity 2018.3
        //Debug.Log("Networkplayer external port: " + Network.player.externalPort);

        Debug.Log("//////////////");
	}
}