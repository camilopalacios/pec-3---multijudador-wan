﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ChatManager2vs2 : NetworkBehaviour
{

    Text TxtTexto;
    InputField inputField;
    string name_player;

    void Start()
    {
        TxtTexto = GameObject.Find("messages").GetComponent<Text>();
        inputField = GameObject.Find("sendMessage").GetComponent<InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
            return;


        if (Input.GetKeyDown(KeyCode.Return))
        {
            name_player = this.GetComponent<Nickname2vs2>().nickname;
            if (inputField.text != "")
            {
                string Mensaje = inputField.text;
                inputField.text = "";

                CmdEnviar(Mensaje, name_player);
            }
        }
    }
    //No queremos que se replique en el servidor para no tener historico al unirse a la partida
    [Command]
    void CmdEnviar(string mensaje, string name_p)
    {
        RpcRecivir(mensaje,name_p);

    }

    [ClientRpc]
    public void RpcRecivir(string mensaje,string name_p)
    {
        TxtTexto.text += name_p + ">>" + mensaje + "\n";
    }
}
