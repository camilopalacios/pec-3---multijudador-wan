﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Nickname2vs2 : NetworkBehaviour {

	public Text nameLabel;	
	string tempName = "";


	[SyncVar(hook = "OnChangeName")]
	public string nickname = "Player";



	//punto 1 : actualizar servidor
	[Command] 
	void CmdChangeName(string newNick) {
		nickname = newNick;
		nameLabel.text = nickname;
	}

	void Start() {
		nameLabel.text = nickname;
	}

	//punto 2 : actualizar clientes
	public void OnChangeName(string newNick) {
		nickname = newNick;
		nameLabel.text = newNick; 
	}

	void OnGUI() {
		if (isLocalPlayer) {
			tempName = GUI.TextField (new Rect(25,15,100,25), tempName);
			if (GUI.Button (new Rect (130, 15, 64, 25), "Change"))
				CmdChangeName (tempName);
		}
	}
}
