﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController2vs2 : MonoBehaviour
{
  private Transform target;
  private float smooth = 20f;   // How smoothly the camera catches up with it's target movement in the x axis.

  // Use this for initialization
  void Start()
  {
    StartCoroutine(FollowPlayer());
  }

  void FixedUpdate()
  {
    if (target)
    {
      MoveCamera();
    }
  }

  IEnumerator FollowPlayer()
  {
    yield return new WaitForSeconds(1.5f);
    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    foreach (GameObject player in players)
    {
      if (player.GetComponent<TankController2vs2>().isLocalPlayer)
      {
        target = player.transform;
      }
    }
  }

  private void MoveCamera()
  {
    // By default the target x and y coordinates of the camera are it's current x and y coordinates.
    float targetX = transform.position.x;
    float targetZ = transform.position.z;
    transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * smooth);
  }
}
