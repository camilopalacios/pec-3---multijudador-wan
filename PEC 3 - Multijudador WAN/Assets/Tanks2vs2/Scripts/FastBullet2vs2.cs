﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastBullet2vs2 : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {

        GameObject hit = collision.gameObject;
        // SI EL IMPACTO DE LA BALA ES CONTRA UN PLAYER DE OTRO EQUIPO
        if (hit.GetComponent<ChangeColor2vs2>() != null && hit.GetComponent<ChangeColor2vs2>().m_PlayerColor != "Rojo")
        {
            // SI EL QUE DISPARA NO ES DEL MISMO EQUIPO, LE HACE DAÑO
            if (this.tag != hit.GetComponent<ChangeColor2vs2>().m_PlayerColor)
            {
                Health2vs2 health = hit.GetComponent<Health2vs2>();
                if (health != null)
                {
                    health.TakeDamage(5);
                }
            }
        }

        Destroy(gameObject);
    }
}
