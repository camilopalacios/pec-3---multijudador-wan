﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CounterManager2vs2 : NetworkBehaviour {
	// DEFINIMOS EL PLAYER PARA PODERLO HEREDAR EN TODOS LOS OTROS SCRIPTS
	public static CounterManager2vs2 singleton;
	public int verdes = 0;
	public int azules = 0;

	[SyncVar(hook = "OnChangeVerdes")]
	public string m_Verdes = "0";

	[SyncVar(hook = "OnChangeAzules")]
	public string m_Azules = "0";

	// Use this for initialization
	void Start () {
		if (CounterManager2vs2.singleton != null) {
			Destroy (this);
		} else {
			singleton = this;
		}
		this.enabled = true;
	}

	void Update() {
		if (isServer) {
			// VA COMPROBANDO QUE LOS CONTADORES SEAN CORRECTOS
			ActualizarContadores ();
		}
	}

	public void ActualizarContadores() {
		// BUSCAMOS CUANTOS JUGADORES HAY DE CADA COLOR PARA ASIGNAR EL COLOR DEL SIGUIENTE
		ChangeColor2vs2[] pc = NetworkManager.FindObjectsOfType<ChangeColor2vs2>();
		verdes = 0;
		azules = 0;

		for (int x = 0; x <= pc.Length - 1; x++) {
			if (pc [x].m_PlayerColor == "Verde") {
				verdes += 1;
			}
			if (pc [x].m_PlayerColor == "Azul") {
				azules += 1;
			}
		}
        m_Verdes = verdes.ToString();
        m_Azules = azules.ToString();
    }


	void OnGUI() {
		//if (isLocalPlayer) {
		// FORMATO DEL TEXTO QUE VAMOS A MOSTRAR
		var TextStyle = new GUIStyle ();
		TextStyle.normal.textColor = Color.white;
		TextStyle.alignment = TextAnchor.MiddleCenter;

		// CREAMOS LA BOTONERA Y EL CONTADOR DE TANQUES VERDES
		GUI.Label (new Rect (200, 15, 25, 25), m_Verdes, TextStyle);
		// CREAMOS LA BOTONERA Y EL CONTADOR DE TANQUES AZULES
		GUI.Label (new Rect (240, 15, 25, 25), m_Azules, TextStyle);

	}

	//punto 2 : actualizar numero verdes en clientes
	public void OnChangeVerdes(string Nombre) {
		m_Verdes = (System.Convert.ToInt32 (Nombre)).ToString ();
	}

	//punto 2 : actualizar numero azules en clientes
	public void OnChangeAzules(string Nombre) {
		m_Azules = (System.Convert.ToInt32 (Nombre)).ToString ();
	}
}
