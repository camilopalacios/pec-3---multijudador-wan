using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class LobbyDiscovery2vs2 : NetworkDiscovery {

    void Awake() {
        Initialize();
    }

    public override void OnReceivedBroadcast(string fromAddress, string data) {
        Debug.Log ("Client discovery received broadcast " + data + " from " + fromAddress);

        base.OnReceivedBroadcast(fromAddress, data);

        if (!NetworkManager.singleton.IsClientConnected())  {
            NetworkManager.singleton.networkAddress = fromAddress;
            NetworkManager.singleton.StartClient();
        }
    }
}