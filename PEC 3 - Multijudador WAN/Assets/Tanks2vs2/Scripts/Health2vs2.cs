using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health2vs2 : NetworkBehaviour {

    public bool destroyOnDeath;

    public const int maxHealth = 100;

    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;

    public RectTransform healthBar;

    private NetworkStartPosition[] spawnPoints;

	public GameObject player2vs2ExplosionPrefab;
	private AudioSource m_ExplosionAudio;               
	private ParticleSystem m_ExplosionParticles;

    void Start()
    {
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();

			// PARA PONER UNA EXPLOSION CUANDO MUERA
			/*// Instantiate the explosion prefab and get a reference to the particle system on it.
			m_ExplosionParticles = Instantiate (player2vs2ExplosionPrefab).GetComponent<ParticleSystem> ();

			// Get a reference to the audio source on the instantiated prefab.
			m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource> ();

			// Disable the prefab so it can be activated when it's required.
			m_ExplosionParticles.gameObject.SetActive (false);*/
        }
    }

    public void TakeDamage(int amount) {
        if (!isServer)  {
            return;
        }

        currentHealth -= amount;
        if (currentHealth <= 0) {
            if (destroyOnDeath) {
				// PARA PONER UNA EXPLOSION CUANDO MUERA
				/*// Move the instantiated explosion prefab to the tank's position and turn it on.
				m_ExplosionParticles.transform.position = transform.position;
				m_ExplosionParticles.gameObject.SetActive (true);

				// Play the particle system of the tank exploding.
				m_ExplosionParticles.Play ();

				// Play the tank explosion sound effect.
				m_ExplosionAudio.Play();*/

				Destroy(gameObject);
            }
            else {
                currentHealth = maxHealth;

                // called on the Server, but invoked on the Clients
                RpcRespawn();
            }
        }
    }

    private void OnChangeHealth(int newHealth) {
        currentHealth = newHealth;
        healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
    }

    [ClientRpc]
    void RpcRespawn() {
        if (isLocalPlayer) {
            // Set the spawn point to origin as a default value
            Vector3 spawnPoint = Vector3.zero;

            // If there is a spawn point array and the array is not empty, pick a spawn point at random
            if (spawnPoints != null && spawnPoints.Length > 0) {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }

            // Set the player's position to the chosen spawn point
            transform.position = spawnPoint;
        }
    }
}