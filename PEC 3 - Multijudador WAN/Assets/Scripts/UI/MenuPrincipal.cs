﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPrincipal : MonoBehaviour {

  public Canvas playerCanvas;

	public void OneVSOne()  {
    	PlayerPrefs.SetInt("Players", 2);
    	gameObject.SetActive(false);
    	playerCanvas.gameObject.SetActive(true);
  	}

  	public void TwoVSTwo() {
		SceneManager.LoadScene("Lobby2vs2");
	}

  	public void AllVSAll() {
		SceneManager.LoadScene("Lobby");
  	}

  	public void GoTo1VS1() {
    	string nickname1 = GameObject.Find("NicknameField1").GetComponent<InputField>().text;
	    if (nickname1 == "") nickname1 = "Player1";
    	PlayerPrefs.SetString("Nickname1", nickname1);
    	string nickname2 = GameObject.Find("NicknameField2").GetComponent<InputField>().text;
    	if (nickname2 == "") nickname2 = "Player2";
    	PlayerPrefs.SetString("Nickname2", nickname2);
    	SceneManager.LoadScene("1vs1");
  	}
}
